package br.ufrn.imd.controllers;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.ufrn.imd.dominio.Comanda;
import br.ufrn.imd.services.ComandaService;

@SessionScoped
@ManagedBean(name = "estatisticaMBean")
public class EstatisticaMBean {

	private ComandaService comandaService;

	private int clientesDia;

	private int pedidosCliente;

	private String consumoCliente;
	
	private String melhorDia;

	/**
	 * Construtor
	 */
	public EstatisticaMBean() {
		comandaService = new ComandaService();
	}

	/**
	 * Diretorio raiz
	 * 
	 * @return
	 */
	public String getDir() {
		return "/pages/gestor";
	}

	public String voltar() {
		return getDir() + "/conta.jsf";
	}

	public String menuMovimento() {
		return getDir() + "/menuEstatisticaMovimento.jsf";
	}

	/**
	 * 
	 * @return
	 */
	public String resultadoGeraisMovimento() {
		this.clientesDia = this.clientesPorDia();
		this.pedidosCliente = this.pedidosPorCliente();
		this.consumoCliente = this.consumoPorCliente();
		this.melhorDia= this.melhorDiaSemana();

		return getDir() + "/estatisticaMovimentoCliente.jsf";

	}

	public String tabelaMovimento() {
		return getDir() + "/tabelaMovimento.jsf";
	}

	/**
	 * Apresenta a m�dia de Clientes por Dia
	 * 
	 * @return
	 */
	public int clientesPorDia() {
		return comandaService.mediaClientesPorDia();
	}

	/**
	 * Apresenta a m�dia de Pedidos por Cliente
	 * 
	 * @return
	 */
	public int pedidosPorCliente() {
		return comandaService.mediaPedidosPorCliente();
	}

	/**
	 * Apresenta a m�dia de valor gasto por Cliente
	 * 
	 * @return
	 */
	public String consumoPorCliente() {
		return String.format("%.2f", comandaService.mediaConsumoPorCliente());
	}

	public String melhorDiaSemana() {
		List<Integer> melhoresDias = new ArrayList<Integer>();
		int maiorMovimento = 0;
		String melhorDia = "";

		// encontra o maior movimento de todos os dias da semana
		for (int i = 1; i <= 7; i++) {
			int movimento = this.clientesPorDiaSemana(i);
			if (movimento > maiorMovimento) {
				maiorMovimento = movimento;
			}
		}
		// verifica se o maior movimento foi igual em mais de um dia da semana
		for (int i = 1; i <= 7; i++) {
			int movimento = this.clientesPorDiaSemana(i);
			if (movimento == maiorMovimento)
				melhoresDias.add(i);
		}

		if(melhoresDias.size() == 7){
			melhorDia = "Todos os dias!";
			return melhorDia;
		}
			
		
		// concatena todos os melhores dias
		for (int i = 0; i < melhoresDias.size(); i++) {
			switch (melhoresDias.get(i)) {
			case 1:
				melhorDia = melhorDia + "domingo ";
				break;
			case 2:
				melhorDia = melhorDia + "segunda-feira ";
				break;
			case 3:
				melhorDia = melhorDia + "ter�a-feira ";
				break;
			case 4:
				melhorDia = melhorDia + "quarta-feira ";
				break;
			case 5:
				melhorDia = melhorDia + "quinta-feira ";
				break;
			case 6:
				melhorDia = melhorDia + "sexta-feira ";
				break;
			case 7:
				melhorDia = melhorDia + "s�bado ";
				break;
			default:
				break;
			}
		}

		return melhorDia;
	}

	/**
	 * Busca a quantidade de Clientes num dia da semana especifico
	 * 
	 * @param dia,
	 *            onde 1=Domingo e 7=S�bado
	 * @return a quantidade de Clientes atendidos num dia especifico da senama
	 */
	public int clientesPorDiaSemana(int dia) {
		List<Comanda> comandas = comandaService.buscarListaComandas();
		Calendar data = Calendar.getInstance();
		int count = 0;

		for (Comanda c : comandas) {
			data.setTime(c.getDataCriacao());
			int diaSemana = data.get(Calendar.DAY_OF_WEEK);
			if (diaSemana == dia) {
				count++;
			}
		}
		return count;
	}

	/**
	 * @return the clientesDia
	 */
	public int getClientesDia() {
		return clientesDia;
	}

	/**
	 * @param clientesDia
	 *            the clientesDia to set
	 */
	public void setClientesDia(int clientesDia) {
		this.clientesDia = clientesDia;
	}

	/**
	 * @return the pedidosCliente
	 */
	public int getPedidosCliente() {
		return pedidosCliente;
	}

	/**
	 * @param pedidosCliente
	 *            the pedidosCliente to set
	 */
	public void setPedidosCliente(int pedidosCliente) {
		this.pedidosCliente = pedidosCliente;
	}

	/**
	 * @return the consumoCliente
	 */
	public String getConsumoCliente() {
		return consumoCliente;
	}

	/**
	 * @param consumoCliente
	 *            the consumoCliente to set
	 */
	public void setConsumoCliente(String consumoCliente) {
		this.consumoCliente = consumoCliente;
	}

	/**
	 * @return the melhorDia
	 */
	public String getMelhorDia() {
		return melhorDia;
	}

	/**
	 * @param melhorDia the melhorDia to set
	 */
	public void setMelhorDia(String melhorDia) {
		this.melhorDia = melhorDia;
	}

}
