package br.ufrn.imd.controllers;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.ufrn.imd.dominio.AvaliacaoServico;
import br.ufrn.imd.dominio.Cliente;
import br.ufrn.imd.services.AvaliacaoServicoService;

/**
 * 
 * ManagedBean para avalia��o de servi�o
 * 
 * @author pdr_m
 *
 */
@ManagedBean(name = "avaliacaoServicoMBean")
@SessionScoped
public class AvaliacaoServicoMBean {
	
	/**
	 * Objeto da avaliacao
	 */
	private AvaliacaoServico avaliacao;
	
	/**
	 * Service da AvaliacaoServico
	 */
	private AvaliacaoServicoService avaliacaoServicoService;

	/**
	 * Construtor da classe
	 */
	public AvaliacaoServicoMBean() {
		super();
		this.avaliacaoServicoService = new AvaliacaoServicoService();
		this.avaliacao = new AvaliacaoServico();
	}

	/**
	 * @return the avaliacao
	 */
	public AvaliacaoServico getAvaliacao() {
		return avaliacao;
	}

	/**
	 * @param avaliacao the avaliacao to set
	 */
	public void setAvaliacao(AvaliacaoServico avaliacao) {
		this.avaliacao = avaliacao;
	}
	
	/**
	 * M�todo para redirecionar a p�gina e submenter a avaliacao para o service
	 * @param avaliador
	 * @return
	 */
	public String avaliar(Cliente avaliador) {
		this.avaliacao.setAvaliador(avaliador);
		
		this.avaliacaoServicoService.salvarAvaliacao(this.avaliacao);
		
		this.avaliacao = new AvaliacaoServico();
		
		return "/pages/cliente/conta.jsf";
	}
	
	/**
	 * Redireciona para a p�gina principal do cliente caso ele opte por n�o avaliar o servico
	 * @return
	 */
	public String naoAvaliar() {
		
		this.avaliacao = new AvaliacaoServico();
		
		return "/pages/cliente/conta.jsf";
	}

	/**
	 * @return the avaliacoes
	 */
	public List<AvaliacaoServico> getAvaliacoes() {
		return this.avaliacaoServicoService.getAvaliacoesDeServico();
	}
	
	/**
	 * Redireciona para a p�gina de visualizar avalia��es
	 * @return
	 */
	public String visualizarAvaliacoes() {
		return "/pages/gestor/visualizarAvaliacoes.jsf";
	}
	
	/**
	 * Volta para a p�gina da conta do gestor
	 * @return
	 */
	public String voltar() {
		return "/pages/gestor/conta.jsf#";
	}
}
