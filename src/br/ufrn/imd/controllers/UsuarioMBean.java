package br.ufrn.imd.controllers;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.ufrn.imd.dominio.Usuario;
import br.ufrn.imd.exceptions.NegocioException;
import br.ufrn.imd.services.UsuarioService;

/**
 * 
 * ManagedBean de Usu�rio
 * 
 * @author Pedro Paulo, Edilvo Lima
 *
 */
@SessionScoped
@ManagedBean(name = "usuarioMBean")
public class UsuarioMBean {
	/**
	 * Service do Usuario
	 */
	private UsuarioService usuarioService;

	/**
	 * Usu�rio object
	 */
	private Usuario obj;

	/**
	 * Usuario logado na sess�o
	 */
	private Usuario usuarioLogado;

	/**
	 * Salvando um usuario antigo para quando for atualizar
	 */
	private Usuario usuarioAntigo;

	/**
	 * Construtor do MBean
	 */
	public UsuarioMBean() {
		this.usuarioService = new UsuarioService();
		this.obj = new Usuario();
	}

	/**
	 * Retorna ao diret�rio padr�o da p�gina
	 * 
	 * @return
	 */
	protected String getDir() {
		return "/pages/usuario";
	}

	/**
	 * Retorna para o form de cria��o de um novo usu�rio
	 */
	public String novo() {
		this.obj = new Usuario();
		return this.getDir() + "/form.jsf";
	}
	
	/**
	 * Salvar um usu�rio e retornar para list de usu�rios
	 * 
	 * @return
	 */
	public String salvar() {
		
		Usuario usuarioParaSalvar = this.obj;
		
		try{ 
			this.usuarioService.salvar(usuarioParaSalvar);
		} catch(NegocioException e) {
			FacesContext.getCurrentInstance().addMessage("formUsuario:loginUsuario", new FacesMessage("", e.getMessage()));
			this.obj = new Usuario();
			return this.getDir() + "/form.jsf";
		}
		
		return this.getDir() + "/list.jsf";
	}
	
	/**
	 * Atualizar o usu�rio
	 * @return
	 */
	public String atualizar() {
		
		boolean atualizou = false;
		
		try{ 
			this.usuarioService.atualizar(this.usuarioAntigo, this.obj);
			atualizou = true;
		} catch(NegocioException e) {
			FacesContext.getCurrentInstance().addMessage("formUsuario:loginUsuario", new FacesMessage("", e.getMessage()));
			this.obj = new Usuario();
			atualizou = false;
		}
		
		if(atualizou)
			return this.getDir() + "/list.jsf";
		else
			return "";
	}

	/**
	 * Editar dados do usu�rio
	 * 
	 * @param usuario
	 * @return
	 */
	public String editar(Usuario usuario) {
		obj = new Usuario();
		obj.clone(usuario);
		this.usuarioAntigo = usuario;
		
		return this.getDir() + "/form.jsf";
	}

	/**
	 * Remover um usu�rio
	 * 
	 * @param usuario
	 * @return
	 */
	public String remover(Usuario usuario) {
		this.usuarioService.remover(obj);
		return this.getDir() + "/list.jsf";
	}

	/**
	 * Visualiza os dados de um usu�rio
	 * 
	 * @param usuario
	 * @return
	 */
	public String visualizar(Usuario usuario) {
		obj = usuario;
		return this.getDir() + "/visualizar.jsf";
	}

	/**
	 * Retorna ao menu principal da aplica��o
	 * 
	 * @return
	 */
	public String menu() {
		return "/index.jsf";
	}

	public String repetirSenha() {

		return this.getDir() + "/form.jsf";
	}

	/**
	 * Retorna o obj Usuario
	 * 
	 * @return the user object
	 */
	public Usuario getObj() {
		return obj;
	}

	/**
	 * Seta o obj Usuario
	 * 
	 * @param obj
	 */
	public void setObj(Usuario obj) {
		this.obj = obj;
	}

	/**
	 * @return the usuarioLogado
	 */
	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	/**
	 * @param usuarioLogado
	 *            the usuarioLogado to set
	 */
	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	/**
	 * Retorna a lista de usuarios
	 * 
	 * @return the users list
	 */
	public List<Usuario> getListaUsuarios() {
		//this.listaUsuarios = this.usuarioService.getListaUsuarios();
		return this.usuarioService.getListaUsuarios();
	}

	/**
	 * Seta a lista de usuarioss
	 * 
	 * @param listaUsuarios
	 */
	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		//this.listaUsuarios = listaUsuarios;
	}

}