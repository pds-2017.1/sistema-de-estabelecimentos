package br.ufrn.imd.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.ufrn.imd.dominio.Estoque;
import br.ufrn.imd.dominio.Produto;
import br.ufrn.imd.services.ProdutoService;

/**
 * MBean de Estoque
 * 
 * @author sergio.luna
 *
 */

@SessionScoped
@ManagedBean(name = "estoqueMBean")
public class EstoqueMBean {

	/**
	 * Service de Produto
	 */
	private ProdutoService produtoService;

	/**
	 * Lista de Produtos
	 */
	private List<Produto> listaProdutos;

	/**
	 * Objeto do tipo Estoque
	 */
	private Estoque obj;

	/**
	 * Construtor Padr�o
	 */
	public EstoqueMBean() {
		obj = new Estoque();
		produtoService = new ProdutoService();
		listaProdutos = new ArrayList<Produto>();
	}

	/**
	 * Redireciona para o view do estoque
	 */
	public String novo() {
		obj = new Estoque();
		return "/pages/estoque/list.jsf";
	}

	/**
	 * @return the listaProdutos
	 */
	public List<Produto> getListaProdutos() {
		listaProdutos = produtoService.buscarListaProdutos();
		return listaProdutos;
	}

	/**
	 * @param listaProdutos
	 *            the listaProdutos to set
	 */
	public void setListaProdutos(List<Produto> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}

	/**
	 * @return the obj
	 */
	public Estoque getObj() {
		return obj;
	}

	/**
	 * @param obj
	 *            the obj to set
	 */
	public void setObj(Estoque obj) {
		this.obj = obj;
	}

}
