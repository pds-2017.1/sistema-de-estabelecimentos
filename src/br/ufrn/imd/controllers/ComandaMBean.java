package br.ufrn.imd.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.ufrn.imd.dominio.Cliente;
import br.ufrn.imd.dominio.Comanda;
import br.ufrn.imd.dominio.Pedido;
import br.ufrn.imd.dominio.Produto;
import br.ufrn.imd.exceptions.NegocioException;
import br.ufrn.imd.services.ClienteService;
import br.ufrn.imd.services.ComandaService;
import br.ufrn.imd.services.PedidoService;
import br.ufrn.imd.services.ProdutoService;

/**
 * MBean de Comanda
 * 
 * @author sergio.luna
 *
 */
@SessionScoped
@ManagedBean(name = "comandaMBean")
public class ComandaMBean {

	/**
	 * Service de Comanda
	 */
	private ComandaService comandaService;

	/**
	 * Service de Cliente
	 */
	private ClienteService clienteService;

	/**
	 * Service de Pedido
	 */
	private PedidoService pedidoService;

	/**
	 * Service de Produto
	 */
	private ProdutoService produtoService;

	/**
	 * Objeto do tipo Comanda
	 */
	private Comanda obj;
	
	/**
	 * Lista de Comandas
	 */
	private List<Comanda> listaComandas;

	/**
	 * Lista de Clientes
	 */
	private List<Cliente> listaClientes;

	/**
	 * Lista de Produtos
	 */
	private List<Produto> listaProdutos;

	/**
	 * Lista de Pedidos
	 */
	private List<Pedido> listaPedidos;

	/**
	 * M�todo construtor
	 */
	public ComandaMBean() {
		obj = new Comanda();
		comandaService = new ComandaService();
		clienteService = new ClienteService();
		pedidoService = new PedidoService();
		produtoService = new ProdutoService();
		listaClientes = new ArrayList<Cliente>();
		listaComandas = new ArrayList<Comanda>();
		listaProdutos = new ArrayList<Produto>();
		listaPedidos = new ArrayList<Pedido>();
	}

	/**
	 * Retornar ao diretorio padr�o da p�gina
	 * 
	 * @return
	 */
	protected String getDir() {
		return "/pages/comanda";
	}

	/**
	 * Salvar um comanda
	 */
	public String salvar() {
		obj.setDataCriacao(Calendar.getInstance().getTime());
		try {
			comandaService.salvar(obj);
		} catch (NegocioException e) {
			e.printStackTrace();
			FacesMessage message = new FacesMessage(e.getMessage());
			FacesContext.getCurrentInstance().addMessage("criarComandaForm:comanda", message);
			
			this.obj.setId(0);
			
			return "";
		}
		return getDir() + "/list.jsf";
	}
	
	/**
	 * Atualizar a comanda
	 * @return
	 */
	public String atualizar() {
		try {
			comandaService.salvar(obj);
		} catch (NegocioException e) {
			e.printStackTrace();
			FacesMessage message = new FacesMessage(e.getMessage());
			FacesContext.getCurrentInstance().addMessage("criarComandaForm:comanda", message);
			
			return "";
		}
		return getDir() + "/list.jsf";
	}
	
	/**
	 * Editar atributos de uma comanda
	 */
	public String editar(Comanda comanda) {
		obj = new Comanda();
		obj.clone(comanda);
		return getDir() + "/form.jsf";
	}

	/**
	 * Remover uma comanda
	 */
	public String remover(Comanda comanda) {
		comandaService.removerComanda(comanda);
		return getDir() + "/list.jsf";
	}

	/**
	 * Visualizar uma comanda
	 */
	public String visualizar(Comanda comanda) {
		obj = comanda;
		return getDir() + "/visualizar.jsf";
	}
	
	/**
	 * Visualizar uma comanda do cliente
	 */
	public String visualizarComandaCliente(Comanda comanda) {
		obj = comanda;
		return "/pages/cliente/visualizarComanda.jsf";
	}

	/**
	 * Redireciona para o preForm atualizar o objeto
	 */
	public String novo() {
		obj = new Comanda();
		return getDir() + "/preForm.jsf";
	}

	/**
	 * Redireciona para o menu principal
	 */
	public String menu() {
		return "/index.jsf";
	}

	/**
	 * Redireciona para o form
	 */
	public String inserirComanda(Cliente cliente) {
		obj.setCliente(cliente);
		return getDir() + "/form.jsf";
	}

	/**
	 * Redirecionar para o finalizarComanda
	 * 
	 * @param comanda
	 * @return
	 */
	public String irParaFinalizarComanda(Comanda comanda) {
		obj = comanda;
		return getDir() + "/finalizarComanda.jsf";
	}
	
	/**
	 * Redirecionar para o finalizarComanda a partir do cliente
	 * 
	 * @param comanda
	 * @return
	 */
	public String irParaFinalizarComandaCliente(Comanda comanda) {
		obj = comanda;
		return "/pages/cliente/finalizarComanda.jsf";
	}

	/**
	 * Finalizar a Comanda
	 * 
	 * @param comanda
	 * @return
	 */
	public String finalizarComanda(Comanda comanda) {
		this.obj = comanda;
		this.comandaService.finalizarComanda(obj);
		return getDir() + "/list.jsf";
	}
	
	/**
	 * Finalizar a Comanda a partir do Cliente
	 * 
	 * @param comanda
	 * @return
	 */
	public String finalizarComandaCliente(Comanda comanda) {
		this.obj = comanda;
		this.comandaService.finalizarComanda(obj);
//		return "/pages/cliente/comanda.jsf";
		return "/pages/cliente/avaliarServico.jsf";
	}

	/**
	 * Verifica se a comanda foi finalizada
	 * 
	 * @param comanda
	 * @return
	 */
	public String verificaComanda(Comanda comanda) {
		if (comanda.isFinalizada() == true) {
			return "Finalizada";
		} else
			return "Aberta";
	}

	/**
	 * @return the listaComandas
	 */
	public List<Comanda> getListaComandas() {
		listaComandas = comandaService.buscarListaComandas();
		return listaComandas;
	}

	/**
	 * @param listaComandas
	 *            the listaComandas to set
	 */
	public void setListaComandas(List<Comanda> listaComandas) {
		this.listaComandas = listaComandas;
	}

	/**
	 * @return the obj
	 */
	public Comanda getObj() {
		return obj;
	}

	/**
	 * @param obj
	 *            the obj to set
	 */
	public void setObj(Comanda obj) {
		this.obj = obj;
	}

	/**
	 * @return the listaClientes
	 */
	public List<Cliente> getListaClientes() {
		listaClientes = clienteService.buscarListaClientes();
		return listaClientes;
	}

	/**
	 * @param listaClientes
	 *            the listaClientes to set
	 */
	public void setListaClientes(List<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	/**
	 * Lista de produtos para o combobox de pedido
	 */
	public List<Produto> getListaProdutosPedidos() {
		listaProdutos = produtoService.buscarListaProdutos();
		return listaProdutos;
	}

	/**
	 * @return the listaProdutos
	 */
	public List<Produto> getListaProdutos() {
		listaProdutos = produtoService.buscarListaProdutos();
		return listaProdutos;
	}

	/**
	 * @param listaProdutos
	 *            the listaProdutos to set
	 */
	public void setListaProdutos(List<Produto> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}

	/**
	 * @return the listaPedidos
	 */
	public List<Pedido> getListaPedidos() {
		//listaPedidos = pedidoService.buscarListaPedidos();
		return obj.getListPedidos();
	}

	/**
	 * @param listaPedidos
	 *            the listaPedidos to set
	 */
	public void setListaPedidos(List<Pedido> listaPedidos) {
		this.listaPedidos = listaPedidos;
	}

}
