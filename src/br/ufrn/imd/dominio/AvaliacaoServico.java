package br.ufrn.imd.dominio;

/**
 * 
 * Classe que representa uma avaliacao de um servico
 * 
 * @author pdr_m
 *
 */
public class AvaliacaoServico {

	/**
	 * A nota dada pelo usuario para o servico
	 */
	private int nota;
	
	/**
	 * Uma texto da avaliacao
	 */
	private String texto;
	
	/**
	 * O cliente que avaliou o servico
	 */
	Cliente avaliador;

	/**
	 * Construtor da classe
	 */
	public AvaliacaoServico() {
		super();
	}

	/**
	 * @return the nota
	 */
	public int getNota() {
		return nota;
	}

	/**
	 * @param nota the nota to set
	 */
	public void setNota(int nota) {
		this.nota = nota;
	}

	/**
	 * @return the texto
	 */
	public String getTexto() {
		return texto;
	}

	/**
	 * @param texto the texto to set
	 */
	public void setTexto(String texto) {
		this.texto = texto;
	}

	/**
	 * @return the avaliador
	 */
	public Cliente getAvaliador() {
		return avaliador;
	}

	/**
	 * @param avaliador the avaliador to set
	 */
	public void setAvaliador(Cliente avaliador) {
		this.avaliador = avaliador;
	}
	
	
	
}
