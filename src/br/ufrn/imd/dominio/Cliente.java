package br.ufrn.imd.dominio;
/**
 * Classe dominio de Cliente
 * @author sergio.luna
 *
 */
public class Cliente extends Usuario{
	
	/**
	 * Nome do cliente
	 */
	private String nome;
	
	/**
	 * Comanda do cliente
	 */
	private Comanda comandaAtual;
	// TODO: Adicionar atributos: cartoes; cartaoAtual.
	
	/**
	 * Par�metro do cliente para definir um limite de consumo para as suas comandas
	 */
	private Double limiteDeConsumo;
	
	/**
	 * Construtor padr�o de Cliente
	 */
	public Cliente(){
		this.limiteDeConsumo = new Double(0.0);
	}
	
	/**
	 * Contrutor tempor�rio
	 * @param nome
	 */
	public Cliente(String nome){
		this();
		this.nome = nome;
	}
	
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the comandaAtual
	 */
	public Comanda getComandaAtual() {
		return comandaAtual;
	}

	/**
	 * @param comandaAtual
	 *            the comandaAtual to set
	 */
	public void setComandaAtual(Comanda comandaAtual) {
		this.comandaAtual = comandaAtual;
	}

	/**
	 * @return the limiteDeConsumo
	 */
	public Double getLimiteDeConsumo() {
		return limiteDeConsumo;
	}

	/**
	 * @param limiteDeConsumo the limiteDeConsumo to set
	 */
	public void setLimiteDeConsumo(Double limiteDeConsumo) {
		this.limiteDeConsumo = limiteDeConsumo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getLogin();
	}
	
	
	

}
