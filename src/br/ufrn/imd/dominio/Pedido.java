package br.ufrn.imd.dominio;
/**
 * Classe dominio de Pedido
 * @author S�rgio
 *
 */
public class Pedido  {
	
	/**
	 * Produto do pedido
	 */
	private Produto produto;
	
	/**
	 * Comanda do Cliente
	 */
	private Comanda comanda;
	
	/**
	 * Valor do Pedido
	 */
	private Double valor;
	
	/**
	 * Quantidade de Produtos por Pedido
	 */
	private int quantidade;
	
	
	/**
	 * Construtor Padr�o
	 * @param produto
	 * @param comanda
	 * @param valor
	 * @param quantidade
	 */
	public Pedido(Produto produto, Comanda comanda, Double valor, int quantidade) {
		super();
		this.produto = produto;
		this.comanda = comanda;
		this.valor = valor;
		this.quantidade = quantidade;
	}

	/**
	 * Construtor Padr�o
	 */
	public Pedido(){
		produto = new Produto();
	}
	
	/**
	 * @return the produto
	 */
	public Produto getProduto() {
		return produto;
	}

	/**
	 * @param produto
	 *            the produto to set
	 */
	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	/**
	 * @return the comanda
	 */
	public Comanda getComanda() {
		return comanda;
	}

	/**
	 * @param comanda
	 *            the comanda to set
	 */
	public void setComanda(Comanda comanda) {
		this.comanda = comanda;
	}

	/**
	 * @return the valor
	 */
	public Double getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(Double valor) {
		this.valor = valor;
	}

	/**
	 * @return the quantidade
	 */
	public int getQuantidade() {
		return quantidade;
	}

	/**
	 * @param quantidade
	 *            the quantidade to set
	 */
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return produto.getDescricao() + "= " + quantidade;
	}

}
