package br.ufrn.imd.dominio;
/**
 * Classe dominio de Produto
 * @author sergio.luna
 *
 */
public class Produto {
	
	/**
	 * Id do produto
	 */
	private int id;
	
	/**
	 * Tipo do produto
	 */
	private String tipo;
	
	/**
	 * Descri��o do produto
	 */
	private String descricao;
	
	/**
	 * Quantidade do produto no estoque
	 */
	private int quantidadeNoEstoque;
	
	/**
	 * Valor do Produto
	 */
	private Double valorProduto;
	
	
	
	/**
	 * Construtor padr�o
	 * @param id
	 * @param tipo
	 * @param descricao
	 * @param quantidadeNoEstoque
	 * @param valorProduto
	 */
	public Produto(int id, String tipo, String descricao, int quantidadeNoEstoque, Double valorProduto) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.descricao = descricao;
		this.quantidadeNoEstoque = quantidadeNoEstoque;
		this.valorProduto = valorProduto;
	}

	/**
	 * Construtor padr�o de Produto
	 */
	public Produto(){
		
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the quantidadeNoEstoque
	 */
	public int getQuantidadeNoEstoque() {
		return quantidadeNoEstoque;
	}

	/**
	 * @param quantidadeNoEstoque
	 *            the quantidadeNoEstoque to set
	 */
	public void setQuantidadeNoEstoque(int quantidadeNoEstoque) {
		this.quantidadeNoEstoque = quantidadeNoEstoque;
	}

	/**
	 * @return the valorProduto
	 */
	public Double getValorProduto() {
		return valorProduto;
	}

	/**
	 * @param valorProduto the valorProduto to set
	 */
	public void setValorProduto(Double valorProduto) {
		this.valorProduto = valorProduto;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return descricao;
	}

}
