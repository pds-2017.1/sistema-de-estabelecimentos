package br.ufrn.imd.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.ufrn.imd.dao.ProdutoDao;
import br.ufrn.imd.dominio.Produto;

/**
 * Converter de Produto
 * @author sergio.luna
 *
 */
@FacesConverter("produtoConverter")
public class ProdutoConverter implements Converter {
	
	private ProdutoDao produtoDao;
	
	public ProdutoConverter(){
		produtoDao = new ProdutoDao();
	}

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		
		if(value == null || value.isEmpty()){
            return null;
        }else{
            Produto produto = new Produto();
            produto = produtoDao.buscarProdutoDescricao(value);
            return produto;
        }
	}

	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if (object != ""){
			Produto produto = (Produto) object;
			return String.valueOf(produto.getDescricao());
		} 
		
		else return null;
	}

}
