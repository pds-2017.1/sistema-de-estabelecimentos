package br.ufrn.imd.services;

import java.util.List;

import br.ufrn.imd.dao.PedidoDao;
import br.ufrn.imd.dominio.Pedido;


/**
 * Service de Pedido
 * @author S�rgio
 *
 */
public class PedidoService {
	
	/**
	 * Dao de pedido para realizar as consultas/persist�ncia de dados
	 */
	private PedidoDao pedidoDao;
	
	/**
	 * Construtor padr�o
	 */
	public PedidoService(){
		pedidoDao = new PedidoDao();
	}
	
	/**
	 * Salvar pedido
	 */
	public void salvar(Pedido pedido){
		pedidoDao.salvar(pedido);
	}
	
	/**
	 * M�todo para buscar a lista de pedidos
	 */
	public List<Pedido> buscarListaPedidos(){
		return pedidoDao.buscarListaPedidos();
	}
	
	/**
	 * Remover pedido da lista de pedidos
	 */
	public void removerPedido(Pedido pedido){
		pedidoDao.removerPedido(pedido);
	}
	
	/**
	 * Adiciona o pedido na comanda
	 * @param pedido
	 */
	public void addPedido(Pedido pedido){
		pedido.getComanda().getListPedidos().add(pedido);
	}

}
