package br.ufrn.imd.services;

import java.util.Iterator;
import java.util.List;

import br.ufrn.imd.dao.UsuarioDao;
import br.ufrn.imd.dominio.Usuario;
import br.ufrn.imd.exceptions.NegocioException;

/**
 * 
 * Service do Usuario
 * 
 * @author Pedro Paulo
 *
 */
public class UsuarioService {

	/**
	 * DAO de usuarios
	 */
	private UsuarioDao usuarioDao;
	

	/**
	 * Construtor da classe
	 */
	public UsuarioService() {
		this.usuarioDao = new UsuarioDao();
	}

	/**
	 * Validar e enviar o usu�rio para inser��o pelo DAO
	 * 
	 * @param obj
	 */
	public void salvar(Usuario usuarioParaSalvar) throws NegocioException{
		boolean podeSalvar = true;
		
		//Utilizando iterador para evitar o problema ConcurrentModificationException
		Iterator<Usuario> usuarioIterador = this.getListaUsuarios().iterator();
		while(usuarioIterador.hasNext() && podeSalvar){
			Usuario usuarioAtualDaLista = usuarioIterador.next();
			if(usuarioAtualDaLista.getLogin().equals(usuarioParaSalvar.getLogin())){
				podeSalvar = false;
				throw new NegocioException("J� existe um usu�rio com o mesmo login.");
			}
		}
		
		if(podeSalvar)
			this.usuarioDao.salvar(usuarioParaSalvar);
	}

	/**
	 * Validar e enviar o usu�rio para remo��o pelo DAO
	 * 
	 * @param obj
	 */
	public void remover(Usuario obj) {
		this.usuarioDao.remover(obj);
	}

	/**
	 * Pedir ao dao a Lista de usu�rios
	 * 
	 * @return
	 */
	public List<Usuario> getListaUsuarios() {
		return this.usuarioDao.getListaUsuarios();
	}

	/**
	 * Atualizar o usu�rio
	 * @param usuarioParaSalvar
	 */
	public void atualizar(Usuario usuarioAntigo, Usuario usuarioAtualizado) throws NegocioException{
		boolean podeAtualizar = true;
		
		for(Usuario user: this.getListaUsuarios() ){
			if(user.getLogin().equals(usuarioAtualizado.getLogin())){
				if(!user.equals(usuarioAntigo)){
					podeAtualizar = false;
					break;
				}
				
			}
		}
		
		if(podeAtualizar){
			this.remover(usuarioAntigo);
			this.usuarioDao.salvar(usuarioAtualizado);
		}else{
			throw new NegocioException("Login j� cadastrado.");
		}
			
		
	}

}
