package br.ufrn.imd.services;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import br.ufrn.imd.dao.ComandaDao;
import br.ufrn.imd.dominio.Cliente;
import br.ufrn.imd.dominio.Comanda;
import br.ufrn.imd.dominio.Pedido;

public class EstatisticaFacade {

	private ComandaDao comandaDao;

	/**
	 * Construtor da Facade
	 */
	public EstatisticaFacade() {
		comandaDao = new ComandaDao();
	}

	/**
	 * @return a m�dia da quantidade de atendimentos (Comandas) pela quantidade de Clientes
	 */
	public int clientesPorDia(){
		List<Comanda> comandaLista = comandaDao.buscarListaComandas();
		ArrayList<Date> diasDistintos = new ArrayList<Date>();
		
		for(Iterator<Comanda> ic = comandaLista.iterator(); ic.hasNext();){
			Comanda c = ic.next();
			if(!diasDistintos.contains(c.getDataCriacao())){
				diasDistintos.add(c.getDataCriacao());
			}
		}
		
		return comandaLista.size() / diasDistintos.size();
	}

	/**
	 * Verifica se um Cliente est� contido numa lista de Clientes
	 * 
	 * @param lista
	 * @param cliente
	 * @return 
	 */
	private boolean isCliente(List<Cliente> lista, Cliente cliente) {
		for (Cliente c : lista) {
			if (c.equals(cliente))
				return true;
		}
		return false;
	}

	/**
	 * 
	 * @return a m�dia da quantidade de pedidos realizados 
	 * pela quantidade de Clientes que realizaram aqueles pedidos
	 */
	public int pedidosPorCliente() {
		List<Comanda> comandaLista = comandaDao.buscarListaComandas();
		List<Cliente> listaCliente = new ArrayList<Cliente>();
		int totalPedidos = 0;

		for (Comanda c : comandaLista) {
			if (!isCliente(listaCliente, c.getCliente())) {
				listaCliente.add(c.getCliente());
			}
			totalPedidos = totalPedidos + c.getListPedidos().size();
		}
		return totalPedidos / listaCliente.size();
	}

	/**
	 * 
	 * @return a m�dia de valores gastos por cada Cliente
	 */
	public double consumoPorCliente() {
		List<Comanda> comandaLista = comandaDao.buscarListaComandas();
		List<Pedido> pedidoLista = new ArrayList<Pedido>();
		List<Cliente> listaCliente = new ArrayList<Cliente>();
		 
		double total = 0.00;

		for (Comanda c : comandaLista) {
			if (!isCliente(listaCliente, c.getCliente())) {
				listaCliente.add(c.getCliente());
				
			}
			total = total + c.getTotal();
//			for (Pedido p : c.getListPedidos()) {
//				pedidoLista.add(p);
//			}
		}
		double valor = 0.00;
//		valor = (double) (pedidoLista.size() / listaCliente.size());
		valor = (double)(total / listaCliente.size());
		
		return valor;
	}
}
