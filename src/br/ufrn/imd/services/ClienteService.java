package br.ufrn.imd.services;

import java.util.List;

import br.ufrn.imd.dao.ClienteDao;
import br.ufrn.imd.dominio.Cliente;

/**
 * Service de Cliente
 * @author sergio.luna
 * 
 */
public class ClienteService {
	
	/**
	 * Dao de cliente para realizar as consultas/persist�ncia de dados
	 */
	private ClienteDao clienteDao;
	
	/**
	 * Construtor padr�o
	 */
	public ClienteService(){
		clienteDao = new ClienteDao();
	}
	
	/**
	 * Salvar cliente
	 */
	public void salvar(Cliente cliente){
		clienteDao.salvar(cliente);
	}
	
	/**
	 * M�todo para buscar a lista de clientes
	 */
	public List<Cliente> buscarListaClientes(){
		return clienteDao.buscarListaClientes();
	}
	
	/**
	 * Remover cliente da lista de clientes
	 */
	public void removerCliente(Cliente cliente){
		clienteDao.removerCliente(cliente);
	}

	/**
	 * Limitar o consumo do cliente
	 * @param obj
	 */
	public void limitarConsumo(Cliente obj) {
		//buscar pelo cliente
		Cliente clienteNoBanco = this.clienteDao.buscarClientePeloLogin(obj.getLogin());
		//atualizar o seu dado
		clienteNoBanco.setLimiteDeConsumo(obj.getLimiteDeConsumo());
		//atualiz�-lo no banco
		this.clienteDao.limitarConsumoDoCliente(clienteNoBanco);
	}

}
