package br.ufrn.imd.exceptions;

/**
 * 
 * Classe de exce��o para definir exce��es de regras de neg�cio
 * 
 * @author pdr_m
 *
 */
public class NegocioException extends Exception{

	public NegocioException() {
		super();
	}

	public NegocioException(String arg0) {
		super(arg0);
	}

}
