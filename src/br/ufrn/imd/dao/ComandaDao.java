package br.ufrn.imd.dao;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import br.ufrn.imd.dominio.Comanda;

/**
 * Dao de Comanda
 * 
 * @author sergio.luna
 *
 */
public class ComandaDao {

	/**
	 * Construtor padr�o
	 */
	public ComandaDao() {

	}

	/**
	 * Salvar comanda
	 */
	public void salvar(Comanda comanda) {
		List<Comanda> listc = BancoDados.getInstance().getComandas();
		for (Iterator<Comanda> i = listc.iterator(); i.hasNext();) {
			Comanda c = i.next();
			if (c.getId() == comanda.getId()) {
				i.remove();
			}
		}
		listc.add(comanda);
	}

	/**
	 * 
	 * @param comanda
	 */
	public void atualizar(Comanda comanda) {
		List<Comanda> listc = BancoDados.getInstance().getComandas();
		int index = 0;
		for (Comanda c : listc) {

			if (c.getId() == comanda.getId()) {
				listc.remove(index);
				listc.add(comanda);
			}
			index++;
		}
	}

	/**
	 * Busca lista de comandas
	 * 
	 * @return
	 */
	public List<Comanda> buscarListaComandas() {
		return BancoDados.getInstance().getComandas();
	}

	/**
	 * Remover comanda da lista de comandas
	 */
	public void removerComanda(Comanda comanda) {
		BancoDados.getInstance().getComandas().remove(comanda);
	}

	/**
	 * Buscar a comanda pelo id
	 */
	public Comanda buscarComandaPorId(int id) {
		List<Comanda> listComanda = this.buscarListaComandas();

		for (Comanda c : listComanda) {
			if (c.getId() == id) {
				return c;
			}
		}

		return null;
	}

	/**
	 * Buscar uma comanda e atualiza
	 */
	public void finalizarComanda(Comanda novaComanda) {
		BancoDados.getInstance().getComandas().set(
				BancoDados.getInstance().getComandas().indexOf(this.buscarComandaPorId(novaComanda.getId())),
				novaComanda);
	}

}
