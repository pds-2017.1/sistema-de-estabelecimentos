package br.ufrn.imd.dao;

import java.util.List;

import br.ufrn.imd.dominio.AvaliacaoServico;

/**
 * 
 * DAO da Avaliacao de Servico
 * 
 * @author pdr_m
 *
 */
public class AvaliacaoServicoDao {

	/**
	 * Construtor da classe
	 */
	public AvaliacaoServicoDao(){
		super();
	}
	
	/**
	 * Salvar a avalia��o na lista do banco de dados
	 * @param avaliacao
	 */
	public void salvarAvaliacao(AvaliacaoServico avaliacao) {
		BancoDados.getInstance().getAvaliacoesDeServico().add(avaliacao);
	}

	/**
	 * Retorna todas a avaliacoes do banco de dados
	 * @return
	 */
	public List<AvaliacaoServico> getAvaliacoesDeServico() {
		return BancoDados.getInstance().getAvaliacoesDeServico();
	}
}
