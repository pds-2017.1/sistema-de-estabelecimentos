package br.ufrn.imd.dao;

import java.util.List;

import br.ufrn.imd.dominio.Pedido;

/**
 * Dao de Pedido
 * @author S�rgio
 *
 */
public class PedidoDao {
	
	/**
	 * Construtor padr�o
	 */
	public PedidoDao(){
		
	}
	
	/**
	 * Salvar pedido
	 */
	public void salvar(Pedido pedido){
		BancoDados.getInstance().getPedidos().add(pedido);
	}
	
	/**
	 * Busca lista de pedidos
	 * @return
	 */
	public List<Pedido> buscarListaPedidos(){
		return BancoDados.getInstance().getPedidos();
	}
	
	/**
	 * Remover pedido da lista de pedidos
	 */
	public void removerPedido(Pedido pedido){
		BancoDados.getInstance().getPedidos().remove(pedido);
	}

}
