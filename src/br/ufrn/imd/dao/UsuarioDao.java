package br.ufrn.imd.dao;

import java.util.List;

import br.ufrn.imd.dominio.Usuario;

/**
 * 
 * DAO do Usuario
 * 
 * @author Pedro Paulo
 *
 */
public class UsuarioDao {

	/**
	 * Construtor da classe
	 */
	public UsuarioDao() {

	}

	/**
	 * Adiciona um usu�rio na lista de usu�rios do banco de dados
	 * @param obj
	 */
	public void salvar(Usuario obj) {
		BancoDados.getInstance().getUsuarios().add(obj);
	}

	/**
	 * Remover um usu�rio da lista de usu�rios do banco de dados
	 * @param obj
	 */
	public void remover(Usuario obj) {
		BancoDados.getInstance().getUsuarios().remove(obj);
	}

	/**
	 * Retorna a lista de usu�rios que est� contida no banco de dados
	 * @return
	 */
	public List<Usuario> getListaUsuarios() {
		return BancoDados.getInstance().getUsuarios();
	}

}
